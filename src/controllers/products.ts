import { Request, Response } from 'express';

import Product from '../models/product';

export const getAddProduct = (req: Request, res: Response): void => {
    res.render('add-product', {
        pageTitle: 'Add product',
        path: '/admin/add-product'
    });
};

export const postAddProduct = (req: Request, res: Response): void => {
    new Product(req.body.title).save();
    res.redirect('/shop');
};
