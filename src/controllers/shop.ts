import { Request, Response } from 'express';

import Product from '../models/product';

export const getShop = (req: Request, res: Response): void => {

    Product.getAll(products => {
        res.render('shop', {
            pageTitle: 'My Shop',
            path: '/shop',
            products
        });
    });
};
