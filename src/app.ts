import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';

import adminRouter from './routes/admin';
import shopRouter from './routes/shop';

import { get404 } from './controllers/404';

import { rootDir } from './utils/path';

const app = express();

app.set('view engine', 'ejs');
app.set('views', 'views');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(rootDir, '..', 'public')));

app.use('/admin', adminRouter);
app.use('/shop', shopRouter);

app.use(get404);

app.listen(3000);
