import express from 'express';

import { getShop } from '../controllers/shop';

const router = express.Router();

router.get('/', getShop);

export default router;
