import { readFile, writeFile } from 'fs';

import { pathToProducts as path } from '../utils/path';

export default class Product {
    title: string;

    constructor(t: string) {
        this.title = t;
    }

    save(): void {
        Product.getAll(products => {
            products.push(this);
            const content = JSON.stringify(products);
            writeFile(path, content, err => console.log(err));
        });
    }

    static getAll(cb: (p: Product[]) => void): void {
        let products: Product[] = [];
        readFile(path, (err, data) => {
            if (!err) {
                products = JSON.parse(data.toString());
            }
            cb(products);
        });
    }
}
