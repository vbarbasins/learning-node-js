import path from 'path';

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
export const rootDir = path.dirname(require.main!.filename);

export const pathToProducts = path.join(
    rootDir,
    '..',
    'data',
    'products.json'
);
