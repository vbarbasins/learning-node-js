module.exports = {
    'ignorePatterns': ['dist/*.js'],
    'env': {
        'es2021': true,
        'node': true
    },
    'extends': [
        'eslint:recommended',
        'plugin:@typescript-eslint/recommended'
    ],
    'parser': '@typescript-eslint/parser',
    'parserOptions': {
        'ecmaVersion': 12,
        'sourceType': 'module'
    },
    'plugins': [
        '@typescript-eslint'
    ],
    'rules': {
        'eol-last': [
            'error',
            'always'
        ],
        'function-paren-newline': [
            'error',
            {
                'minItems': 4
            }
        ],
        'indent': [
            'error',
            4
        ],
        'linebreak-style': [
            'error',
            'unix'
        ],
        'max-len': [
            'error',
            {
                'code': 80
            }
        ],
        'no-trailing-spaces': [
            'error'
        ],
        'quotes': [
            'error',
            'single'
        ],
        'semi': [
            'error',
            'always'
        ],
        '@typescript-eslint/explicit-function-return-type': [
            'error'
        ]
    }
};
